# frozen_string_literal: true

require 'bundler'
require 'rspec/core/rake_task'
require 'rubocop/rake_task'
require 'rake_compiler_dock'
require 'rake/extensiontask'
require 'rb_sys'

gemspec = Gem::Specification.load(File.expand_path('gitlab-glfm-markdown.gemspec', __dir__))

ruby_cc_version = RakeCompilerDock.ruby_cc_version(">= 3.1")
cross_platforms = %w[
  aarch64-linux-gnu
  aarch64-linux-musl
  arm64-darwin
  x86_64-darwin
  x86_64-linux-gnu
  x86_64-linux-musl
]

desc 'Default: run specs'
task default: [:spec]

Bundler::GemHelper.install_tasks

desc 'Publish gem to RubyGems.org'
task :publish_gem do |_t|
  gem = GemPublisher.publish_if_updated('gitlab-glfm-markdown.gemspec', :rubygems)
  puts "Published #{gem}" if gem
end

desc 'Start a console session for the gem'
task :console do
  exec 'bin/console'
end

RSpec::Core::RakeTask.new(:spec)

desc 'Lint code'
RuboCop::RakeTask.new

task build: :compile # rubocop:disable Rake/Desc
task test: :spec # rubocop:disable Rake/Desc

Gem::PackageTask.new(gemspec)

Rake::ExtensionTask.new('glfm_markdown', gemspec) do |ext|
  ext.lib_dir = 'lib/glfm_markdown'
  ext.source_pattern = '*.{rs,toml}'
  ext.cross_compile = true
  ext.cross_platform = cross_platforms
end

namespace 'gem' do
  task 'prepare' do  # rubocop:disable Rake/Desc
    sh 'bundle'
  end

  desc 'Build the native gem for the local architecture (non-docker)'
  task 'native' do
    sh 'rake native gem'
  end

  cross_platforms.each do |plat|
    desc "Build the native gem for #{plat}"
    task plat => 'prepare' do
      ENV["RCD_IMAGE"] = "rbsys/#{plat}:#{RbSys::VERSION}"

      RakeCompilerDock.sh <<~SH, platform: plat
        bundle && \
        RUBY_CC_VERSION="#{ruby_cc_version}" \
        bundle exec rake native:#{plat} pkg/#{gemspec.full_name}-#{plat}.gem
      SH
    end
  end
end

CLEAN.include('lib/glfm_markdown/glfm_markdown.bundle')
CLOBBER.include('pkg')
CLOBBER.include('target')
