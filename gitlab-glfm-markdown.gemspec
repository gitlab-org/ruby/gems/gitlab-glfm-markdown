# frozen_string_literal: true

require_relative 'lib/glfm_markdown/version'

Gem::Specification.new do |spec|
  spec.name = 'gitlab-glfm-markdown'
  spec.version = GLFMMarkdown::VERSION
  spec.authors = ['Brett Walker']
  spec.email = ['bwalker@gitlab.com']

  spec.summary = 'GLFM Markdown'
  spec.description = 'Markdown processing for GitLab Flavored Markdown'
  spec.homepage = 'https://gitlab.com/gitlab-org/ruby/gems/gitlab-glfm-markdown'
  spec.license = 'MIT'

  spec.required_ruby_version = '>= 3.1'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  spec.metadata['changelog_uri'] = "#{spec.homepage}/-/releases"

  spec.files = ['LICENSE', 'README.md', 'Cargo.lock']
  spec.files += Dir.glob('lib/**/*.rb')
  spec.files += Dir.glob('ext/**/*.{rs,toml,lock,rb}')

  spec.require_paths = ['lib']
  spec.extensions = ['ext/glfm_markdown/extconf.rb']

  spec.add_dependency 'rb_sys', '~> 0.9.109'

  spec.add_development_dependency 'rake', '~> 13.0'
  spec.add_development_dependency 'rake-compiler', '~> 1.2'
end
