import init, { render_xml_with_options, render_with_options, get_default_options } from '../pkg/glfm_markdown_wasm.js';

await init();

export function parseAndRender() {
  let render_options = get_default_options();

  // extensions
  const alerts = document.querySelector("#alerts").checked;
  const autolink = document.querySelector("#autolink").checked;
  const description_lists = document.querySelector("#description_lists").checked;
  const footnotes = document.querySelector("#footnotes").checked;
  const front_matter_delimiter = "---";
  const gemojis = document.querySelector("#gemojis").checked;
  const greentext = document.querySelector("#greentext").checked;
  const header_ids = "user-content-";
  const math_code = document.querySelector("#math_code").checked;
  const math_dollars = document.querySelector("#math_dollars").checked;
  const multiline_block_quotes = document.querySelector("#multiline_block_quotes").checked;
  const spoiler = document.querySelector("#spoiler").checked;
  const strikethrough = document.querySelector("#strikethrough").checked;
  const subscript = document.querySelector("#subscript").checked;
  const superscript = document.querySelector("#superscript").checked;
  const table = document.querySelector("#table").checked;
  const tagfilter = document.querySelector("#tagfilter").checked;
  const tasklist = document.querySelector("#tasklist").checked;
  const underline = document.querySelector("#underline").checked;
  const wikilinks_title_after_pipe = document.querySelector("#wikilinks_title_after_pipe").checked;
  const wikilinks_title_before_pipe = document.querySelector("#wikilinks_title_before_pipe").checked;

  render_options.alerts = alerts;
  render_options.autolink = autolink;
  render_options.description_lists = description_lists;
  render_options.footnotes = footnotes;
  if (document.querySelector("#front_matter_delimiter").checked) {
    render_options.front_matter_delimiter = front_matter_delimiter;
  }
  render_options.gemojis = gemojis;
  render_options.greentext = greentext;
  if (document.querySelector("#header_ids").checked) {
    render_options.header_ids = header_ids;
  }
  render_options.math_code = math_code;
  render_options.math_dollars = math_dollars;
  render_options.multiline_block_quotes = multiline_block_quotes;
  render_options.spoiler = spoiler;
  render_options.strikethrough = strikethrough;
  render_options.subscript = subscript;
  render_options.superscript = superscript;
  render_options.table = table;
  render_options.tagfilter = tagfilter;
  render_options.tasklist = tasklist;
  render_options.underline = underline;
  render_options.wikilinks_title_after_pipe = wikilinks_title_after_pipe;
  render_options.wikilinks_title_before_pipe = wikilinks_title_before_pipe;

  // render
  const escape = document.querySelector("#escape").checked;
  const escaped_char_spans = document.querySelector("#escaped_char_spans").checked;
  const figure_with_caption = document.querySelector("#figure_with_caption").checked;
  const full_info_string = document.querySelector("#full_info_string").checked;
  const gfm_quirks = document.querySelector("#gfm_quirks").checked;
  const github_pre_lang = document.querySelector("#github_pre_lang").checked;
  const hardbreaks = document.querySelector("#hardbreaks").checked;
  const ignore_empty_links = document.querySelector("#ignore_empty_links").checked;
  const ignore_setext = document.querySelector("#ignore_setext").checked;
  const sourcepos = document.querySelector("#sourcepos").checked;
  const experimental_inline_sourcepos = document.querySelector("#experimental_inline_sourcepos").checked;
  const unsafe_ = document.querySelector("#unsafe_").checked;

  render_options.escape = escape;
  render_options.escaped_char_spans = escaped_char_spans;
  render_options.figure_with_caption = figure_with_caption;
  render_options.full_info_string = full_info_string;
  render_options.gfm_quirks = gfm_quirks;
  render_options.github_pre_lang = github_pre_lang;
  render_options.hardbreaks = hardbreaks;
  render_options.ignore_empty_links = ignore_empty_links;
  render_options.ignore_setext = ignore_setext;
  render_options.sourcepos = sourcepos;
  render_options.experimental_inline_sourcepos = experimental_inline_sourcepos;
  render_options.unsafe_ = unsafe_;

  // parse
  const default_info_string = "ruby";
  const relaxed_autolinks = document.querySelector("#relaxed_autolinks").checked;
  const relaxed_tasklist_character = document.querySelector("#relaxed_tasklist_character").checked;
  const smart = document.querySelector("#smart").checked;

  if (document.querySelector("#default_info_string").checked) {
    render_options.default_info_string = default_info_string;
  }
  render_options.relaxed_autolinks = relaxed_autolinks;
  render_options.relaxed_tasklist_character = relaxed_tasklist_character;
  render_options.smart = smart;

  const textarea = document.querySelector("#markdown");
  const markdown = textarea.value;
  const result = render_with_options(markdown, render_options);

  const preview_iframe = document.querySelector("#preview_iframe");
  preview_iframe.contentDocument.querySelector("body").innerHTML = result;

  const html_code = document.querySelector("#htmlpre #html")
  html_code.innerText = result;

  const result_xml = render_xml_with_options(markdown, render_options);
  const ast_xml = document.querySelector("#astpre #ast")
  ast_xml.innerText = result_xml;

  // markSelection();
}

export function load() {
  const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
  const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))

  const textarea = document.querySelector("#markdown");
  const params = new Proxy(new URLSearchParams(window.location.search), {
    get: (searchParams, prop) => searchParams.get(prop),
  });

  // var smartSelected = getQueryVariable("smart") === "1";
  // $("#smart").prop("checked", smartSelected);
  // reader.options.smart = smartSelected;

  let initial_text = params.text;
  if (initial_text) {
    textarea.value = initial_text;
  }

  parseAndRender();

  document.querySelector("#clear-text-box").addEventListener("click", () => {
    textarea.value = "";
    parseAndRender();
  });

  document.querySelector("#permalink").addEventListener("click", () => {
    window.location.pathname = "/index.html";
    window.location.search =
        "text=" +
        encodeURIComponent(textarea.value);

    // options are not included in the permalink yet, but when they are
    // the `unsafe` option should be excluded to avoid a reflected XSS vulnerability

    // var smart = $("#smart").prop("checked");
    // window.location.search =
    //     "text=" +
    //     encodeURIComponent(textarea.val()) +
    //     (smart ? "&smart=1" : "");
  });

  // extensions
  document.querySelector("#alerts").addEventListener("change", parseAndRender);
  document.querySelector("#autolink").addEventListener("change", parseAndRender);
  document.querySelector("#description_lists").addEventListener("change", parseAndRender);
  document.querySelector("#footnotes").addEventListener("change", parseAndRender);
  document.querySelector("#front_matter_delimiter").addEventListener("change", parseAndRender);
  document.querySelector("#gemojis").addEventListener("change", parseAndRender);
  document.querySelector("#greentext").addEventListener("change", parseAndRender);
  document.querySelector("#header_ids").addEventListener("change", parseAndRender);
  document.querySelector("#math_code").addEventListener("change", parseAndRender);
  document.querySelector("#math_dollars").addEventListener("change", parseAndRender);
  document.querySelector("#multiline_block_quotes").addEventListener("change", parseAndRender);
  document.querySelector("#spoiler").addEventListener("change", parseAndRender);
  document.querySelector("#strikethrough").addEventListener("change", parseAndRender);
  document.querySelector("#subscript").addEventListener("change", parseAndRender);
  document.querySelector("#superscript").addEventListener("change", parseAndRender);
  document.querySelector("#table").addEventListener("change", parseAndRender);
  document.querySelector("#tagfilter").addEventListener("change", parseAndRender);
  document.querySelector("#tasklist").addEventListener("change", parseAndRender);
  document.querySelector("#underline").addEventListener("change", parseAndRender);
  document.querySelector("#wikilinks_title_after_pipe").addEventListener("change", parseAndRender);
  document.querySelector("#wikilinks_title_before_pipe").addEventListener("change", parseAndRender);

  // render
  document.querySelector("#escape").addEventListener("change", parseAndRender);
  document.querySelector("#escaped_char_spans").addEventListener("change", parseAndRender);
  document.querySelector("#figure_with_caption").addEventListener("change", parseAndRender);
  document.querySelector("#full_info_string").addEventListener("change", parseAndRender);
  document.querySelector("#gfm_quirks").addEventListener("change", parseAndRender);
  document.querySelector("#github_pre_lang").addEventListener("change", parseAndRender);
  document.querySelector("#hardbreaks").addEventListener("change", parseAndRender);
  document.querySelector("#ignore_empty_links").addEventListener("change", parseAndRender);
  document.querySelector("#ignore_setext").addEventListener("change", parseAndRender);
  document.querySelector("#sourcepos").addEventListener("change", parseAndRender);
  document.querySelector("#experimental_inline_sourcepos").addEventListener("change", parseAndRender);
  document.querySelector("#unsafe_").addEventListener("change", parseAndRender);

  // parse
  document.querySelector("#default_info_string").addEventListener("change", parseAndRender);
  document.querySelector("#relaxed_autolinks").addEventListener("change", parseAndRender);
  document.querySelector("#relaxed_tasklist_character").addEventListener("change", parseAndRender);
  document.querySelector("#smart").addEventListener("change", parseAndRender);

  textarea.addEventListener("input", updateText);
  // textarea.addEventListener("keydown", markSelection);
  // textarea.addEventListener("click", markSelection);
  // textarea.addEventListener("focus", markSelection);
}

function updateText(e) {
  parseAndRender();
}

// function markSelection() {
//   const textarea = document.querySelector("#markdown");
//   const cursorPos = textarea.selectionStart;
//
//   // now count newline up to this pos
//   const textval = textarea.value
//   let lineNumber = 1;
//
//   for (let i = 0; i < cursorPos; i++) {
//     if (textval.charAt(i) === "\n") {
//         lineNumber++;
//     }
//   }
//
//   const preview = document.querySelector("#preview_iframe")
//     .contentDocument
//     .querySelector("body");
//   const nodelist = preview.querySelectorAll("[data-sourcepos^='" + lineNumber + ":']");
//   if (nodelist.length > 0) {
//     const elt = nodelist[nodelist.length - 1];
//     const selected = preview.querySelector(".selected");
//     if (selected) {
//       selected.classList.remove("selected");
//     }
//     elt.classList.add("selected");
//   }
// };
