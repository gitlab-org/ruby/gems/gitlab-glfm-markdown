use serde::{Deserialize, Serialize};
use wasm_bindgen::prelude::*;

extern crate console_error_panic_hook;
use std::panic;

// Called when the wasm module is instantiated
#[wasm_bindgen(start)]
pub fn main() -> Result<(), JsValue> {
    panic::set_hook(Box::new(console_error_panic_hook::hook));

    // Use `web_sys`'s global `window` function to get a handle on the global
    // window object.
    // let window = web_sys::window().expect("no global `window` exists");
    // let document = window.document().expect("should have a document on window");
    // let _body = document.body().expect("document should have a body");

    // Manufacture the element we're gonna append
    // let val = document.create_element("p")?;
    // val.set_inner_html("Hello from Rust!");
    // body.append_child(&val)?;

    Ok(())
}

#[derive(Serialize, Deserialize)]
pub struct RenderOptions {
    pub alerts: bool,
    pub autolink: bool,
    pub default_info_string: Option<String>,
    pub description_lists: bool,
    pub escape: bool,
    pub escaped_char_spans: bool,
    pub figure_with_caption: bool,
    pub footnotes: bool,
    pub front_matter_delimiter: Option<String>,
    pub full_info_string: bool,
    pub gemojis: bool,
    pub gfm_quirks: bool,
    pub github_pre_lang: bool,
    pub greentext: bool,
    pub hardbreaks: bool,
    pub header_ids: Option<String>,
    pub ignore_empty_links: bool,
    pub ignore_setext: bool,
    pub math_code: bool,
    pub math_dollars: bool,
    pub multiline_block_quotes: bool,
    pub relaxed_autolinks: bool,
    pub relaxed_tasklist_character: bool,
    pub sourcepos: bool,
    pub experimental_inline_sourcepos: bool,
    pub smart: bool,
    pub spoiler: bool,
    pub strikethrough: bool,
    pub subscript: bool,
    pub superscript: bool,
    //pub syntax_highlighting: this would need to be Option<&dyn SyntaxHighlighterAdapter>,
    pub table: bool,
    pub tagfilter: bool,
    pub tasklist: bool,
    pub underline: bool,
    pub unsafe_: bool,
    pub wikilinks_title_after_pipe: bool,
    pub wikilinks_title_before_pipe: bool,

    pub debug: bool,
}

#[wasm_bindgen]
pub fn get_default_options() -> JsValue {
    let render_options = default_render_options();

    serde_wasm_bindgen::to_value(&render_options).unwrap()
}

#[wasm_bindgen]
pub fn render(text: String) -> String {
    comrak::markdown_to_html(&text, &comrak::Options::default())
}

#[wasm_bindgen]
pub fn render_with_options(text: String, options: JsValue) -> String {
    let overrides: RenderOptions = serde_wasm_bindgen::from_value(options).unwrap();
    let comrak_options = render_options_to_comrak_options(overrides);

    comrak::markdown_to_html(&text, &comrak_options)
}

#[wasm_bindgen]
pub fn render_xml(text: String) -> String {
    comrak::markdown_to_commonmark_xml(&text, &comrak::Options::default())
}

#[wasm_bindgen]
pub fn render_xml_with_options(text: String, options: JsValue) -> String {
    let overrides: RenderOptions = serde_wasm_bindgen::from_value(options).unwrap();
    let comrak_options = render_options_to_comrak_options(overrides);

    comrak::markdown_to_commonmark_xml(&text, &comrak_options)
}

fn default_render_options() -> RenderOptions {
    let comrak_options = comrak::Options::default();

    RenderOptions {
        alerts: comrak_options.extension.alerts,
        autolink: comrak_options.extension.autolink,
        default_info_string: None,
        description_lists: comrak_options.extension.description_lists,
        escape: comrak_options.render.escape,
        escaped_char_spans: comrak_options.render.escaped_char_spans,
        figure_with_caption: comrak_options.render.figure_with_caption,
        footnotes: comrak_options.extension.footnotes,
        front_matter_delimiter: None,
        full_info_string: comrak_options.render.full_info_string,
        gemojis: comrak_options.extension.shortcodes,
        gfm_quirks: comrak_options.render.gfm_quirks,
        github_pre_lang: comrak_options.render.github_pre_lang,
        greentext: comrak_options.extension.greentext,
        hardbreaks: comrak_options.render.hardbreaks,
        header_ids: None,
        ignore_empty_links: comrak_options.render.ignore_empty_links,
        ignore_setext: comrak_options.render.ignore_setext,
        math_code: comrak_options.extension.math_code,
        math_dollars: comrak_options.extension.math_dollars,
        multiline_block_quotes: comrak_options.extension.multiline_block_quotes,
        relaxed_autolinks: comrak_options.parse.relaxed_autolinks,
        relaxed_tasklist_character: comrak_options.parse.relaxed_tasklist_matching,
        sourcepos: comrak_options.render.sourcepos,
        experimental_inline_sourcepos: comrak_options.render.experimental_inline_sourcepos,
        smart: comrak_options.parse.smart,
        spoiler: comrak_options.extension.spoiler,
        strikethrough: comrak_options.extension.strikethrough,
        subscript: comrak_options.extension.subscript,
        superscript: comrak_options.extension.superscript,
        //syntax_highlighting: None,
        table: comrak_options.extension.table,
        tagfilter: comrak_options.extension.tagfilter,
        tasklist: comrak_options.extension.tasklist,
        underline: comrak_options.extension.underline,
        unsafe_: comrak_options.render.unsafe_,
        wikilinks_title_after_pipe: comrak_options.extension.wikilinks_title_after_pipe,
        wikilinks_title_before_pipe: comrak_options.extension.wikilinks_title_before_pipe,
        debug: false,
    }
}

fn render_options_to_comrak_options<'c>(options: RenderOptions) -> comrak::Options<'c> {
    let mut comrak_options = comrak::Options::default();

    comrak_options.extension.alerts = options.alerts;
    comrak_options.extension.autolink = options.autolink;
    comrak_options.extension.description_lists = options.description_lists;
    comrak_options.extension.footnotes = options.footnotes;
    comrak_options.extension.front_matter_delimiter = options.front_matter_delimiter;
    comrak_options.extension.greentext = options.greentext;
    comrak_options.extension.header_ids = options.header_ids;
    comrak_options.extension.math_code = options.math_code;
    comrak_options.extension.math_dollars = options.math_dollars;
    comrak_options.extension.multiline_block_quotes = options.multiline_block_quotes;
    comrak_options.extension.shortcodes = options.gemojis;
    comrak_options.extension.spoiler = options.spoiler;
    comrak_options.extension.strikethrough = options.strikethrough;
    comrak_options.extension.subscript = options.subscript;
    comrak_options.extension.superscript = options.superscript;
    comrak_options.extension.table = options.table;
    comrak_options.extension.tagfilter = options.tagfilter;
    comrak_options.extension.tasklist = options.tasklist;
    comrak_options.extension.underline = options.underline;
    comrak_options.extension.wikilinks_title_after_pipe = options.wikilinks_title_after_pipe;
    comrak_options.extension.wikilinks_title_before_pipe = options.wikilinks_title_before_pipe;

    comrak_options.render.escape = options.escape;
    comrak_options.render.escaped_char_spans = options.escaped_char_spans;
    comrak_options.render.figure_with_caption = options.figure_with_caption;
    comrak_options.render.full_info_string = options.full_info_string;
    comrak_options.render.gfm_quirks = options.gfm_quirks;
    comrak_options.render.github_pre_lang = options.github_pre_lang;
    comrak_options.render.hardbreaks = options.hardbreaks;
    comrak_options.render.ignore_empty_links = options.ignore_empty_links;
    comrak_options.render.ignore_setext = options.ignore_setext;
    comrak_options.render.sourcepos = options.sourcepos;
    comrak_options.render.experimental_inline_sourcepos = options.experimental_inline_sourcepos;
    //comrak_options.render.syntax_highlighting = options.syntax_highlighting;

    comrak_options.render.unsafe_ = options.unsafe_;

    comrak_options.parse.default_info_string = options.default_info_string;
    comrak_options.parse.relaxed_autolinks = options.relaxed_autolinks;
    comrak_options.parse.relaxed_tasklist_matching = options.relaxed_tasklist_character;
    comrak_options.parse.smart = options.smart;

    comrak_options
}
