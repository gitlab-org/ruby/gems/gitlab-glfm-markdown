## Gitlab Flavored Markdown dingus

This site provides a way to test out using GLFM. This is currently experimental
and doesn't support all GLFM syntax. All CommonMark and GFM syntax is supported.

Pages site is at:

### Building and running

Requirements:

- `wasm-pack`, installed with `cargo install wasm-pack`
- `basic-http-server`, installed with `cargo install basic-http-server`
(for running locally - you can use any local webserver).

To build the WASM files:

```
cd dingus
rake build
```

Afterwards you can run the `basic-http-server`, which will run the site locally on `http://127.0.0.1:4000`

```
rake server
```
