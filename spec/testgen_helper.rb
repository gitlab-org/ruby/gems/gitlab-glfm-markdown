# frozen_string_literal: true

FIXTURES_DIR = File.expand_path('fixtures', __dir__)
DISABLED_OPTIONS = {
  autolink: false,
  escaped_char_spans: false,
  footnotes: false,
  full_info_string: false,
  github_pre_lang: false,
  hardbreaks: false,
  math_code: false,
  math_dollars: false,
  multiline_block_quotes: false,
  relaxed_autolinks: false,
  sourcepos: false,
  smart: false,
  strikethrough: false,
  table: false,
  tagfilter: false,
  tasklist: false,
  unsafe: true,

  debug: false
}.freeze

def load_spec_file(filename)
  line_number = 0
  start_line = 0
  end_line = 0
  example_number = 0
  markdown_lines = []
  html_lines = []
  state = :text
  headertext = ''
  tests = []
  extensions = []

  header_re = Regexp.new('#+ ')
  filepath = File.join(FIXTURES_DIR, filename)

  File.readlines(filepath, encoding: 'utf-8').each do |line|
    line_number += 1
    l = line.strip

    if l =~ /^`{32} example(.*)$/
      state = :markdown
      extensions = Regexp.last_match(1).split
    elsif l == '`' * 32
      state = :text
      example_number += 1
      end_line = line_number
      tests << {
        markdown: markdown_lines.join.tr('→', "\t"),
        html: html_lines.join.tr('→', "\t").rstrip,
        example: example_number,
        start_line: start_line,
        end_line: end_line,
        section: "#{headertext}#{extensions.to_s unless extensions.empty?}",
        extensions: extensions.map(&:to_sym)
      }
      start_line = 0
      markdown_lines = []
      html_lines = []
    elsif l == '.'
      state = :html
    elsif state == :markdown
      start_line = line_number - 1 if start_line.zero?
      markdown_lines << line
    elsif state == :html
      html_lines << line
    elsif state == :text && header_re.match(line)
      headertext = line.sub(header_re, '').strip
    end
  end

  tests
end

def define_testcase(testcase, options = {})
  test_name = <<~NAME
    #{testcase[:filename]} #{testcase[:section]} (#{testcase[:example]}/#{testcase[:start_line]}-#{testcase[:end_line]}) with markdown:\n#{testcase[:markdown]}
  NAME

  it test_name do
    skip('disabled') if testcase[:extensions].include?(:disabled)

    testcase[:extensions].each_with_object(options) { |extension, options| options[extension] = true }

    unless testcase[:html] == '<IGNORE>'
      expect(GLFMMarkdown.to_html(testcase[:markdown], options: DISABLED_OPTIONS.merge(options)).rstrip)
        .to eq testcase[:html]
    end
  end
end
