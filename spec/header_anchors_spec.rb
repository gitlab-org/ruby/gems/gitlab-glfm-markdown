# frozen_string_literal: true

require 'spec_helper'
require 'nokogiri'

RSpec.describe 'header anchors' do
  it 'does nothing when header_ids is nil' do
    markdown = '# Header'
    rendered = render(markdown)

    expect(rendered).to eq "<h1>Header</h1>\n"
  end

  it 'does nothing with empty headers' do
    markdown = '#'
    rendered = render(markdown)

    expect(rendered).to eq "<h1></h1>\n"
  end

  1.upto(6) do |i|
    it "processes h#{i} elements" do
      markdown = "#{'#' * i} Header #{i}"
      doc = filter(markdown)

      expect(doc.css("h#{i} a").first.attr('id')).to eq "user-content-header-#{i}"
    end
  end

  describe 'anchor tag' do
    let(:markdown_header) { '# Header' }

    it 'has an `anchor` class' do
      doc = filter(markdown_header)

      expect(doc.css('h1 a').first.attr('class')).to eq 'anchor'
    end

    it 'has a namespaced id' do
      doc = filter(markdown_header)

      expect(doc.css('h1 a').first.attr('id')).to eq 'user-content-header'
    end

    it 'links to the non-namespaced id' do
      doc = filter(markdown_header)

      expect(doc.css('h1 a').first.attr('href')).to eq '#header'
    end

    describe 'generated IDs' do
      it 'translates spaces to dashes' do
        doc = filter('# This header has spaces in it')

        expect(doc.css('h1 a').first.attr('href')).to eq '#this-header-has-spaces-in-it'
      end

      it 'squeezes multiple spaces and dashes' do
        doc = filter('# This---header     is poorly-formatted')

        expect(doc.css('h1 a').first.attr('href')).to eq '#this---header-----is-poorly-formatted'
      end

      it 'removes punctuation' do
        doc = filter('# This, header! is, filled. with @ punctuation?')

        expect(doc.css('h1 a').first.attr('href')).to eq '#this-header-is-filled-with--punctuation'
      end

      it 'appends a unique number to duplicates' do
        doc = filter("# One\n## One")

        expect(doc.css('h1 a').first.attr('href')).to eq '#one'
        expect(doc.css('h2 a').first.attr('href')).to eq '#one-1'
      end

      it 'supports Unicode' do
        doc = filter('# 한글')

        expect(doc.css('h1 a').first.attr('id')).to eq 'user-content-한글'
      end
    end
  end

  def render(markdown, header_ids: nil)
    GLFMMarkdown.to_html(markdown, options: { sourcepos: false, header_ids: header_ids })
  end

  def parse(html)
    Nokogiri::HTML::DocumentFragment.parse(html)
  end

  def filter(markdown)
    html = render(markdown, header_ids: 'user-content-')
    parse(html)
  end
end
