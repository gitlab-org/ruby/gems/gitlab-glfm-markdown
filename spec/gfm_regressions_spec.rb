# frozen_string_literal: true

# rubocop:disable RSpec/EmptyExampleGroup
RSpec.describe 'GFM Regression' do
  describe 'regression.txt' do
    tests = load_spec_file('cmark-gfm/regression.txt')

    tests.each do |testcase|
      define_testcase(testcase, { unsafe: true })
    end
  end
end
# rubocop:enable RSpec/EmptyExampleGroup
