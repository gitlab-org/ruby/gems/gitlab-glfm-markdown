# frozen_string_literal: true

RSpec.describe GLFMMarkdown do
  it 'has a version number' do
    expect(described_class::VERSION).not_to be_nil
  end

  context 'with incorrect parameters' do
    it 'requires markdown to be a String' do
      markdown = ['string']

      expect { described_class.to_html(markdown) }.to raise_error(TypeError, 'markdown must be a String')
    end

    it 'requires options to be a Hash' do
      markdown = 'String'

      expect { described_class.to_html(markdown, options: []) }.to raise_error(TypeError, 'options must be a Hash')
    end
  end

  context 'with default GLFM settings' do
    it 'autolinks is true' do
      markdown = '<http://foo.bar.baz>'
      rendered = described_class.to_html(markdown, options: { glfm: true })

      expect(rendered).to have_tag('a', with: { href: 'http://foo.bar.baz' })
    end

    it 'footnotes is true' do
      markdown = <<~MARKDOWN
        This is some text![^1].

        [^1]: Some *bolded* footnote definition.
      MARKDOWN

      rendered = described_class.to_html(markdown, options: { glfm: true })

      expect(rendered).to have_tag('section')
      expect(rendered).to have_tag('p', text: /This is some text!/) do
        with_tag('sup')
      end
    end

    it 'github_pre_lang is false' do
      markdown = <<~MARKDOWN
        ```ruby
        module Foo
        ```
      MARKDOWN

      rendered = described_class.to_html(markdown, options: { glfm: true })

      expect(rendered).to have_tag('code', with: { class: 'language-ruby' })
    end

    it 'hardbreaks is false' do
      markdown = "This and\nthat"
      rendered = described_class.to_html(markdown, options: { glfm: true })

      expect(rendered).to have_tag('p', text: "This and\nthat")
      expect(rendered).not_to have_tag('br')
    end

    it 'multiline_block_quotes is true' do
      markdown = <<~MARKDOWN
        >>>
        This and that
        >>>
      MARKDOWN

      rendered = described_class.to_html(markdown, options: { glfm: true })

      expect(rendered).to have_tag('blockquote', text: 'This and that')
    end

    it 'sourcepos is true' do
      markdown = 'This _and_ that'
      rendered = described_class.to_html(markdown, options: { glfm: true })

      expect(rendered).to have_tag('p', with: { 'data-sourcepos': '1:1-1:15' })
      expect(rendered).to have_tag('em', with: { 'data-sourcepos': '1:6-1:10' })
    end

    it 'smart is false' do
      markdown = 'This "and" that'
      rendered = described_class.to_html(markdown, options: { glfm: true })

      expect(rendered).to have_tag('p', text: 'This "and" that')
      expect(rendered).not_to have_tag('p', text: 'This “and” that')
    end

    it 'strikethrough is true' do
      markdown = 'A proper ~strikethrough~.'
      rendered = described_class.to_html(markdown, options: { glfm: true })

      expect(rendered).to have_tag('del', text: 'strikethrough')
    end

    it 'table is true' do
      markdown = <<~MARKDOWN
        | one | two |
        | --- | --- |
        | 1   | 2   |
      MARKDOWN

      rendered = described_class.to_html(markdown, options: { glfm: true })

      expect(rendered).to have_tag('table')
      expect(rendered).to have_tag('thead')
      expect(rendered).to have_tag('tr')
      expect(rendered).to have_tag('td')
    end

    it 'tagfilter is false' do
      markdown = '<strong> <title> <style>'
      rendered = described_class.to_html(markdown, options: { glfm: true })

      expect(rendered).to have_tag('strong')
      expect(rendered).to have_tag('title')
      expect(rendered).to have_tag('style')
    end

    it 'tasklist is true' do
      markdown = <<~MARKDOWN
      - [ ] one
      MARKDOWN

      rendered = described_class.to_html(markdown, options: { glfm: true })

      expect(rendered).to have_tag('ul')
      expect(rendered).to have_tag('li')
      expect(rendered).to have_tag('input', with: { type: 'checkbox' })
    end

    it 'unsafe is true' do
      markdown = '<script>'
      rendered = described_class.to_html(markdown, options: { glfm: true }).strip

      expect(rendered).to have_tag('script')
      expect(rendered).not_to eq '<!-- raw HTML omitted -->'
    end
  end

  context 'with frozen options' do
    let(:options) { { tagfilter: true }.freeze }

    it 'does not error' do
      markdown = 'This _and_ that'
      rendered = described_class.to_html(markdown, options: options)

      expect { rendered }.not_to raise_error
    end
  end

  it 'turns off sourcepos' do
    markdown = '# header'
    rendered = described_class.to_html(markdown, options: { glfm: true, sourcepos: false })

    expect(rendered).to eq(%(<h1>header</h1>\n))
  end

  describe 'header anchors' do
    it 'has no anchors by default' do
      markdown = '# This and that'
      rendered = described_class.to_html(markdown, options: { sourcepos: false })

      expect(rendered).to eq(%(<h1>This and that</h1>\n))
    end

    it 'generates anchors with no prefix' do
      markdown = '# This and that'
      rendered = described_class.to_html(markdown, options: { sourcepos: false, header_ids: '' })

      expect(rendered)
        .to eq(
          <<~HTML
          <h1><a href="#this-and-that" aria-hidden="true" class="anchor" id="this-and-that"></a>This and that</h1>
          HTML
        )
    end

    it 'generates anchors with specified prefix' do
      markdown = '# This and that'
      rendered = described_class.to_html(markdown, options: { sourcepos: false, header_ids: 'note123-' })

      expect(rendered)
        .to eq(
          <<~HTML
          <h1><a href="#this-and-that" aria-hidden="true" class="anchor" id="note123-this-and-that"></a>This and that</h1>
          HTML
        )
    end
  end
end
