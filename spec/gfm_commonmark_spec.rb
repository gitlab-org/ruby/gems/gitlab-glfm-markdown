# frozen_string_literal: true

# rubocop:disable RSpec/EmptyExampleGroup
RSpec.describe 'GFM Commonmark Spec' do
  tests = load_spec_file('cmark-gfm/spec.txt')

  tests.each do |testcase|
    define_testcase(testcase, { gfm_quirks: true })
  end
end
# rubocop:enable RSpec/EmptyExampleGroup
