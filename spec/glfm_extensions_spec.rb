# frozen_string_literal: true

# rubocop:disable RSpec/EmptyExampleGroup
RSpec.describe 'GLFM Extensions' do
  describe 'multiline_blockquote.md' do
    tests = load_spec_file('glfm-spec/multiline_blockquote.md')

    tests.each do |testcase|
      define_testcase(testcase,
        GLFMMarkdown::GLFM_DEFAULT_OPTIONS.merge({ sourcepos: false, multiline_block_quotes: true }))
    end
  end

  describe 'math_code.md' do
    tests = load_spec_file('glfm-spec/math_code.md')

    tests.each do |testcase|
      define_testcase(testcase, GLFMMarkdown::GLFM_DEFAULT_OPTIONS.merge({ sourcepos: false, math_code: true }))
    end
  end

  describe 'math_dollars.md' do
    tests = load_spec_file('glfm-spec/math_dollars.md')

    tests.each do |testcase|
      define_testcase(testcase, GLFMMarkdown::GLFM_DEFAULT_OPTIONS.merge({ sourcepos: false, math_dollars: true }))
    end
  end

  describe 'wikilinks_title_after_pipe.md' do
    tests = load_spec_file('glfm-spec/wikilinks_title_after_pipe.md')

    tests.each do |testcase|
      define_testcase(testcase,
        GLFMMarkdown::GLFM_DEFAULT_OPTIONS.merge({ sourcepos: false, wikilinks_title_after_pipe: true }))
    end
  end

  describe 'wikilinks_title_before_pipe.md' do
    tests = load_spec_file('glfm-spec/wikilinks_title_before_pipe.md')

    tests.each do |testcase|
      define_testcase(testcase,
        GLFMMarkdown::GLFM_DEFAULT_OPTIONS.merge({ sourcepos: false, wikilinks_title_before_pipe: true }))
    end
  end

  describe 'description_lists.md' do
    tests = load_spec_file('glfm-spec/description_lists.md')

    tests.each do |testcase|
      define_testcase(testcase,
        GLFMMarkdown::GLFM_DEFAULT_OPTIONS.merge({ sourcepos: false, description_lists: true }))
    end
  end

  describe 'alerts.md' do
    tests = load_spec_file('glfm-spec/alerts.md')

    tests.each do |testcase|
      define_testcase(testcase,
        GLFMMarkdown::GLFM_DEFAULT_OPTIONS.merge({ sourcepos: false, alerts: true }))
    end
  end

  describe 'multiline_alerts.md' do
    tests = load_spec_file('glfm-spec/multiline_alerts.md')

    tests.each do |testcase|
      define_testcase(testcase,
        GLFMMarkdown::GLFM_DEFAULT_OPTIONS.merge({ sourcepos: false, alerts: true, multiline_block_quotes: true }))
    end
  end
end
# rubocop:enable RSpec/EmptyExampleGroup
