# frozen_string_literal: true

# rubocop:disable RSpec/EmptyExampleGroup
RSpec.describe 'GFM Extensions' do
  options = {
    autolink: true,
    footnotes: true,
    strikethrough: true,
    table: true,
    tagfilter: true,
    tasklist: true
  }

  describe 'extensions.txt' do
    tests = load_spec_file('cmark-gfm/extensions.txt')

    tests.each do |testcase|
      define_testcase(testcase, options)
    end
  end

  describe 'smart_punct.txt' do
    tests = load_spec_file('cmark-gfm/smart_punct.txt')

    tests.each do |testcase|
      define_testcase(testcase, { smart: true })
    end
  end

  # TODO: In general these tests pass. However for some reason,
  #       different runs can cause the attributes to be in a different
  #       order, failing the test. Disable for now.
  # describe 'extensions-full-info-string.txt' do
  #   tests = load_spec_file('cmark-gfm/extensions-full-info-string.txt')
  #
  #   tests.each do |testcase|
  #     define_testcase(testcase, { full_info_string: true })
  #   end
  # end
end
# rubocop:enable RSpec/EmptyExampleGroup
