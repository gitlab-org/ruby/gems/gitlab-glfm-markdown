# frozen_string_literal: true

require 'spec_helper'
require 'nokogiri'
require 'erb'

PUNCTUATION_PAIRS = {
  "'" => "'",
  '"' => '"',
  ')' => '(',
  ']' => '[',
  '}' => '{'
}.freeze

# Moved over from GitLab codebase
RSpec.describe 'autolinks' do
  using RSpec::Parameterized::TableSyntax

  let(:link) { 'http://about.gitlab.com/' }
  let(:quotes) { ['"', "'"] }

  it 'does nothing when :autolink is false' do
    act = link
    exp = "<p>#{link}</p>\n"

    expect(filter(act, autolink: false).to_html).to eq exp
  end

  it 'does nothing with non-link text' do
    act = 'This text contains no links to autolink'
    exp = "<p>This text contains no links to autolink</p>\n"

    expect(filter(act).to_html).to eq exp
  end

  context 'with various schemes' do
    it 'autolinks http' do
      doc = filter("See #{link}")

      expect(doc.at_css('a').text).to eq link
      expect(doc.at_css('a')['href']).to eq link
    end

    it 'autolinks https' do
      link = 'https://google.com/'
      doc = filter("See #{link}")

      expect(doc.at_css('a').text).to eq link
      expect(doc.at_css('a')['href']).to eq link
    end

    it 'autolinks ftp' do
      link = 'ftp://ftp.us.debian.org/debian/'
      doc = filter("See #{link}")

      expect(doc.at_css('a').text).to eq link
      expect(doc.at_css('a')['href']).to eq link
    end

    it 'autolinks short URLs' do
      link = 'http://localhost:3000/'
      doc = filter("See #{link}")

      expect(doc.at_css('a').text).to eq link
      expect(doc.at_css('a')['href']).to eq link
    end

    it 'autolinks multiple URLs' do
      link1 = 'http://localhost:3000/'
      link2 = 'http://google.com/'

      doc = filter("See #{link1} and #{link2}")

      found_links = doc.css('a')

      expect(found_links.size).to eq(2)
      expect(found_links[0].text).to eq(link1)
      expect(found_links[0]['href']).to eq(link1)
      expect(found_links[1].text).to eq(link2)
      expect(found_links[1]['href']).to eq(link2)
    end

    it 'autolinks smb' do
      link = 'smb:///Volumes/shared/foo.pdf'
      doc = filter("See #{link}")

      expect(doc.at_css('a').text).to eq link
      expect(doc.at_css('a')['href']).to eq link
    end

    it 'autolinks multiple occurrences of smb' do
      link1 = 'smb:///Volumes/shared/foo.pdf'
      link2 = 'smb:///Volumes/shared/bar.pdf'

      doc = filter("See #{link1} and #{link2}")

      found_links = doc.css('a')

      expect(found_links.size).to eq(2)
      expect(found_links[0].text).to eq(link1)
      expect(found_links[0]['href']).to eq(link1)
      expect(found_links[1].text).to eq(link2)
      expect(found_links[1]['href']).to eq(link2)
    end

    it 'autolinks irc' do
      link = 'irc://irc.freenode.net/git'
      doc = filter("See #{link}")

      expect(doc.at_css('a').text).to eq link
      expect(doc.at_css('a')['href']).to eq link
    end

    it 'autolinks rdar' do
      link = 'rdar://localhost.com/blah'
      doc = filter("See #{link}")

      expect(doc.at_css('a').text).to eq link
      expect(doc.at_css('a')['href']).to eq link
    end

    it 'does not include trailing punctuation' do
      ['.', ', ok?', '...', '?', '!', ': is that ok?'].each do |trailing_punctuation|
        doc = filter("See #{link}#{trailing_punctuation}")
        expect(doc.at_css('a').text).to eq link
      end
    end

    it 'includes trailing punctuation when part of a balanced pair' do
      PUNCTUATION_PAIRS.each do |close, open|
        next if quotes.include?(open)

        balanced_link = "#{link}#{open}abc#{close}"
        balanced_actual = filter("See #{balanced_link}...")
        unbalanced_link = "#{link}#{close}"
        unbalanced_actual = filter("See #{unbalanced_link}...")

        expect(balanced_actual.at_css('a').text).to eq(balanced_link)
        expect(balanced_actual.to_html).to include('</a>...')
        expect(unbalanced_actual.at_css('a').text).to eq(link)
        expect(unbalanced_actual.to_html).to include("</a>#{close}...")
      end
    end

    it 'removes trailing quotes' do
      quotes.each do |quote|
        balanced_link = "#{link}#{quote}abc#{quote}"
        balanced_actual = filter("See #{balanced_link}...")
        unbalanced_link = "#{link}#{quote}"
        unbalanced_actual = filter("See #{unbalanced_link}...")

        expect(balanced_actual.at_css('a').text).to eq(balanced_link[0...-1])
        expect(balanced_actual.to_html).to include("</a>#{quote}...")
        expect(unbalanced_actual.at_css('a').text).to eq(link)
        expect(unbalanced_actual.to_html).to include("</a>#{quote}...")
      end
    end

    it 'removes one closing punctuation mark when the punctuation in the link is unbalanced' do
      complicated_link = "(#{link}(a'b[c'd]))'"
      expected_complicated_link = %{<p>(<a href="#{link}(a'b%5Bc'd%5D)">#{link}(a'b[c'd])</a>)'</p>\n}
      actual = filter(complicated_link).to_html

      expect(actual).to eq(expected_complicated_link)
    end

    it 'does double-encode HTML entities' do
      encoded_link = "#{link}?foo=bar&amp;baz=quux"
      expected_encoded_link =
        %(<p><a href="#{link}?foo=bar&amp;amp;baz=quux">#{link}?foo=bar&amp;amp;baz=quux</a></p>\n)
      actual = filter(encoded_link).to_html

      expect(actual).to eq(expected_encoded_link)
    end

    it 'does not include trailing HTML entities' do
      doc = filter("See &lt;&lt;&lt;#{link}&gt;&gt;&gt;")

      expect(doc.at_css('a')['href']).to eq link
      expect(doc.text).to eq "See <<<#{link}>>>\n"
    end

    it 'escapes RTLO and other characters' do
      # rendered text looks like "http://example.com/evilexe.mp3"
      evil_link = "#{link}evil\u202E3pm.exe"
      doc = filter(evil_link.to_s)

      expect(doc.at_css('a')['href']).to eq "http://about.gitlab.com/evil%E2%80%AE3pm.exe"
    end

    it 'encodes international domains' do
      link     = "http://one😄two.com"
      expected = "http://one%F0%9F%98%84two.com"
      doc      = filter(link)

      expect(doc.at_css('a')['href']).to eq expected
    end
  end

  it 'protects against malicious backtracking' do
    doc = "http://#{'&' * 1_000_000}x"

    expect do
      Timeout.timeout(30) { filter(doc) }
    end.not_to raise_error
  end

  it 'does not timeout with excessively long scheme' do
    doc = "#{'h' * 1_000_000}://example.com"

    expect do
      Timeout.timeout(30) { filter(doc) }
    end.not_to raise_error
  end

  # rubocop:disable Layout/LineLength
  describe 'relaxed autolinks' do
    where(:text, :expected) do
      '[https://foo.com]'                 | %(<p>[<a href="https://foo.com">https://foo.com</a>]</p>\n)
      '[[https://foo.com]]'               | %(<p>[[<a href="https://foo.com">https://foo.com</a>]]</p>\n)
      '[http://foo.com/](url)'            | %(<p><a href="url">http://foo.com/</a></p>\n)
      '[[Foo|https://foo.com]]'           | %(<p>[[Foo|<a href="https://foo.com">https://foo.com</a>]]</p>\n)
      '[<https://foo.com>]'               | %(<p>[<a href="https://foo.com">https://foo.com</a>]</p>\n)
      '[http://foo.com/](url)'            | %(<p><a href="url">http://foo.com/</a></p>\n)
      '[http://foo.com/](url'             | %[<p>[http://foo.com/](url</p>\n]
      '[www.foo.com/](url)'               | %(<p><a href="url">www.foo.com/</a></p>\n)
      '{https://foo.com}'                 | %(<p>{<a href="https://foo.com">https://foo.com</a>}</p>\n)
      '[this http://and.com that](url)'   | %(<p><a href="url">this http://and.com that</a></p>\n)
      '[this <http://and.com> that](url)' | %(<p><a href="url">this http://and.com that</a></p>\n)
      '{this http://and.com that}(url)'   | %(<p>{this <a href="http://and.com">http://and.com</a> that}(url)</p>\n)
      %([http://foo.com](url)\n[http://bar.com]\n\n[http://bar.com]: http://bar.com/extra) | %(<p><a href="url">http://foo.com</a>\n<a href="http://bar.com/extra">http://bar.com</a></p>\n)
    end

    with_them do
      it 'renders correctly' do
        doc = filter(text)

        expect(doc.to_html).to eq(expected)
      end
    end
  end
  # rubocop:enable Layout/LineLength

  def render(markdown, options = {})
    GLFMMarkdown.to_html(markdown, options: options)
  end

  def parse(html)
    Nokogiri::HTML::DocumentFragment.parse(html)
  end

  def filter(markdown, options = { gfm_quirks: true, sourcepos: false, autolink: true, relaxed_autolinks: true })
    html = render(markdown, options)
    parse(html)
  end
end
