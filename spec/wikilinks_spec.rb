# frozen_string_literal: true

require 'spec_helper'
require 'nokogiri'

RSpec.describe 'wikilinks' do
  context 'with sanitization of HTML entities' do
    it 'does not unescape HTML entities' do
      doc = filter('This is [[a link|&lt;script&gt;alert(0)&lt;/script&gt;]]')

      expect(doc.at_css('a').text).to eq 'a link'
      expect(doc.at_css('a')['href']).to eq '%3Cscript%3Ealert(0)%3C/script%3E'
    end

    it 'does not unescape HTML entities in the link text' do
      doc = filter('This is [[&lt;script&gt;alert(0)&lt;/script&gt;|link]]')

      expect(doc.at_css('a')['href']).to eq 'link'
      expect(doc.to_html.rstrip).to end_with '>&lt;script&gt;alert(0)&lt;/script&gt;</a></p>'
    end

    it 'does not unescape HTML entities outside the link text' do
      doc = filter('This is &lt;script&gt;alert(0)&lt;/script&gt; [[a link|link]]')

      # This is <script>alert(0)</script> <a href="link">a link</a>

      expect(doc.to_html).to start_with '<p>This is &lt;script&gt;alert(0)&lt;/script&gt; <a href'
    end
  end

  it 'sanitizes the href attribute (case 1)' do
    tag = '[[a|http:\'"injected=attribute&gt;&lt;img/src="0"onerror="alert(0)"&gt;https://example.com]]'
    doc = filter("See #{tag}")

    expect(doc.at_css('a')['href']).to eq 'http:\'%22injected=attribute%3E%3Cimg/src=%220%22onerror=%22alert(0)%22%3Ehttps://example.com'
  end

  # rubocop:disable Layout/LineLength -- test data in this format
  it 'sanitizes the href attribute (case 2)' do
    tag = '<i>[[a|\'"&gt;&lt;svg&gt;&lt;i/class=gl-show-field-errors&gt;&lt;input/title="&lt;script&gt;alert(0)&lt;/script&gt;"/&gt;&lt;/svg&gt;https://example.com]]'
    doc = filter("See #{tag}")

    expect(doc.at_css('i a')['href']).to eq "'%22%3E%3Csvg%3E%3Ci/class=gl-show-field-errors%3E%3Cinput/title=%22%3Cscript%3Ealert(0)%3C/script%3E%22/%3E%3C/svg%3Ehttps://example.com"
  end
  # rubocop:enable Layout/LineLength

  def render(markdown)
    GLFMMarkdown.to_html(markdown, options: { sourcepos: false, wikilinks_title_before_pipe: true, unsafe: true })
  end

  def filter(markdown)
    html = render(markdown)

    Nokogiri::HTML::DocumentFragment.parse(html)
  end
end
