# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'description_lists' do
  using RSpec::Parameterized::TableSyntax

  context 'when invalid lists' do
    where(:text, :result) do
      ':'     | "<p>:</p>\n"
      ': foo' | "<p>: foo</p>\n"
      "a\n:"  | "<p>a\n:</p>\n"
    end

    with_them do
      it 'handles invalid syntax' do
        doc = filter(text)

        expect(doc.to_html).to eq result
      end
    end

    it 'handles invalid with sublist' do
      markdown = <<~MARKDOWN
        - foo
        - : bar
          - baz
      MARKDOWN

      expected = <<~HTML
        <ul>
        <li>foo</li>
        <li>: bar
        <ul>
        <li>baz</li>
        </ul>
        </li>
        </ul>
      HTML

      doc = filter(markdown)

      expect(doc.to_html).to eq expected
    end
  end

  def render(markdown)
    GLFMMarkdown.to_html(markdown, options: { sourcepos: false, description_lists: true, unsafe: true })
  end

  def filter(markdown)
    html = render(markdown)

    Nokogiri::HTML::DocumentFragment.parse(html)
  end
end
