# frozen_string_literal: true

require 'spec_helper'
require 'nokogiri'
require 'timeout'

# Moved over from GitLab codebase. It mirrors many of the tests already in the
# Rust code at https://github.com/kivikakk/comrak/blob/main/src/tests/math.rs
RSpec.describe 'math' do
  using RSpec::Parameterized::TableSyntax

  shared_examples 'inline dollar math' do
    it 'renders correctly' do
      doc = filter(text)

      expected = result_template.gsub('<math>', '<span data-math-style="inline">')
      expected.gsub!('</math>', '</span>')

      expect(doc.to_s.rstrip).to eq expected
    end
  end

  shared_examples 'display dollar math' do
    it 'renders correctly' do
      doc = filter(text)

      expected = result_template.gsub('<math>', '<span data-math-style="display">')
      expected.gsub!('</math>', '</span>')

      expect(doc.to_s.rstrip).to eq expected
    end
  end

  shared_examples 'inline code math' do
    it 'renders correctly' do
      doc = filter(text)

      expected = result_template.gsub('<math>', '<code data-math-style="inline">')
      expected.gsub!('</math>', '</code>')

      expect(doc.to_s.rstrip).to eq expected
    end
  end

  describe 'inline dollar math, using $...$ syntax' do
    context 'with valid syntax' do
      where(:text, :result_template) do
        '$2+2$'                                  | '<p><math>2+2</math></p>'
        '$22+1$ and $22 + a^2$'                  | '<p><math>22+1</math> and <math>22 + a^2</math></p>'
        '$22 and $2+2$'                          | '<p>$22 and <math>2+2</math></p>'
        '$2+2$ $22 and flightjs/Flight$22 $2+2$' | '<p><math>2+2</math> $22 and flightjs/Flight$22 <math>2+2</math></p>'
        '$1/2$ &lt;b&gt;test&lt;/b&gt;'          | '<p><math>1/2</math> &lt;b&gt;test&lt;/b&gt;</p>'
        '$a!$'                                   | '<p><math>a!</math></p>'
        '$x$'                                    | '<p><math>x</math></p>'
        '$1+2\$$'                                | '<p><math>1+2\$</math></p>'
        '$1+\$2$'                                | '<p><math>1+\$2</math></p>'
        '$1+\%2$'                                | '<p><math>1+\%2</math></p>'
        '$1+\#2$'                                | '<p><math>1+\#2</math></p>'
        '$1+\&2$'                                | '<p><math>1+\&amp;2</math></p>'
        '$1+\{2$'                                | '<p><math>1+\{2</math></p>'
        '$1+\}2$'                                | '<p><math>1+\}2</math></p>'
        '$1+\_2$'                                | '<p><math>1+\_2</math></p>'
      end

      with_them do
        it_behaves_like 'inline dollar math'
      end
    end
  end

  # rubocop:disable Layout/LineLength
  describe 'inline display dollar math, using $$...$$ syntax' do
    context 'with valid syntax' do
      where(:text, :result_template) do
        '$$2+2$$'                                    | '<p><math>2+2</math></p>'
        '$$   2+2  $$'                               | '<p><math>   2+2  </math></p>'
        '$$22+1$$ and $$22 + a^2$$'                  | '<p><math>22+1</math> and <math>22 + a^2</math></p>'
        '$22 and $$2+2$$'                            | '<p>$22 and <math>2+2</math></p>'
        '$$2+2$$ $22 and flightjs/Flight$22 $$2+2$$' | '<p><math>2+2</math> $22 and flightjs/Flight$22 <math>2+2</math></p>'
        'flightjs/Flight$22 and $$a^2 + b^2 = c^2$$' | '<p>flightjs/Flight$22 and <math>a^2 + b^2 = c^2</math></p>'
        '$$a!$$'                                     | '<p><math>a!</math></p>'
        '$$x$$'                                      | '<p><math>x</math></p>'
        '$$20,000 and $$30,000'                      | '<p><math>20,000 and </math>30,000</p>'
      end

      with_them do
        it_behaves_like 'display dollar math'
      end
    end
  end
  # rubocop:enable Layout/LineLength

  describe 'block display dollar math using $$\n...\n$$ syntax' do
    context 'with valid syntax' do
      where(:text, :result_template) do
        "$$\n2+2\n$$"      | "<p><math>\n2+2\n</math></p>"
        "$$  \n2+2\n$$"    | "<p><math>  \n2+2\n</math></p>"
        "$$\n2+2\n3+4\n$$" | "<p><math>\n2+2\n3+4\n</math></p>"
      end

      with_them do
        it_behaves_like 'display dollar math'
      end
    end

    context 'when it spans multiple lines' do
      let(:math) do
        <<~MATH
         \\begin{align*}
         \\Delta t   \\frac{d(b_i, a_i)}{c} + \\Delta t_{b_i}
         \\end{align*}
        MATH
      end

      let(:text) { "$$\n#{math}$$" }
      let(:result_template) { "<p><math>\n#{math}</math></p>" }

      it_behaves_like 'display dollar math'
    end

    context 'when it contains \\' do
      let(:math) do
        <<~MATH
          E = mc^2 \\\\
          E = \\$mc^2
        MATH
      end

      let(:text) { "$$\n#{math}$$" }
      let(:result_template) { "<p><math>\n#{math}</math></p>" }

      it_behaves_like 'display dollar math'
    end
  end

  # rubocop:disable Layout/LineLength
  describe 'inline code math using $`...`$ syntax' do
    context 'with valid syntax' do
      where(:text, :result_template) do
        '$`2+2`$'                                    | '<p><math>2+2</math></p>'
        '$`22+1`$ and $`22 + a^2`$'                  | '<p><math>22+1</math> and <math>22 + a^2</math></p>'
        '$22 and $`2+2`$'                            | '<p>$22 and <math>2+2</math></p>'
        '$`2+2`$ $22 and flightjs/Flight$22 $`2+2`$' | '<p><math>2+2</math> $22 and flightjs/Flight$22 <math>2+2</math></p>'
        '$`1+\$2`$'                                  | '<p><math>1+\$2</math></p>'
      end

      with_them do
        it_behaves_like 'inline code math'
      end
    end
  end
  # rubocop:enable Layout/LineLength

  describe 'display math using ```math...``` syntax' do
    it 'adds data-math-style display attribute to display math' do
      expected = <<~HTML
        <pre><code class="language-math" data-math-style="display">2+2
        </code></pre>
      HTML

      doc = filter("```math\n2+2\n```")

      expect(doc.to_s).to eq(expected)
    end

    it 'ignores code blocks that are not math' do
      expected = <<~HTML
        <pre><code class="language-plaintext">2+2
        </code></pre>
      HTML

      doc = filter("```plaintext\n2+2\n```")

      expect(doc.to_s).to eq expected
    end
  end

  it 'handles multiple styles in one text block' do
    doc = filter('$`2+2`$ + $3+3$ + $$4+4$$')

    expect(doc.search('[data-math-style="inline"]').count).to eq(2)
    expect(doc.search('[data-math-style="display"]').count).to eq(1)
  end

  describe 'unrecognized syntax' do
    where(:text, :expected) do
      '`2+2`'               | '<p><code>2+2</code></p>'
      'test $`2+2` test'    | '<p>test $<code>2+2</code> test</p>'
      'test `2+2`$ test'    | '<p>test <code>2+2</code>$ test</p>'
      '$20,000 and $30,000' | '<p>$20,000 and $30,000</p>'
      '$20,000 in $USD'     | '<p>$20,000 in $USD</p>'
      '$ a^2 $'             | '<p>$ a^2 $</p>'
      "$\n$"                | "<p>$\n$</p>"
      '$$$'                 | '<p>$$$</p>'
      '`$1+2$`'             | '<p><code>$1+2$</code></p>'
      '`$$1+2$$`'           | '<p><code>$$1+2$$</code></p>'
      '`$\$1+2$$`'          | '<p><code>$\$1+2$$</code></p>'
    end

    with_them do
      it 'is ignored' do
        doc = filter(text)

        expect(doc.to_s.rstrip).to eq(expected.strip)
      end
    end
  end

  it 'protects against malicious backtracking' do
    expect do
      Timeout.timeout(3) { filter("$$#{' ' * 1_000_000}$") }
    end.not_to raise_error
  end

  def render(markdown)
    GLFMMarkdown.to_html(markdown, options: { sourcepos: false, math_code: true, math_dollars: true })
  end

  def parse(html)
    Nokogiri::HTML::DocumentFragment.parse(html)
  end

  def filter(markdown)
    html = render(markdown)
    parse(html)
  end
end
