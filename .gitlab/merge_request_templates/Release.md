<!-- Replace `<PREVIOUS_VERSION>` with the previous version number here, `<COMMIT_UPDATING_VERSION>` with the latest
commit from this merge request, and `<NEW_VERSION>` with the upcoming version number. -->

## Diff

https://gitlab.com/gitlab-org/ruby/gems/gitlab-glfm-markdown/-/compare/<PREVIOUS_VERSION>...<COMMIT_UPDATING_VERSION>

## Checklist

- [ ] Update the versions according to [SemVer](https://semver.org)
  - [ ] `lib/glfm_markdown/version.rb`
  - [ ] `ext/glfm_markdown/Cargo.toml`
- [ ] Update _"Currently using..."_ in `README.md` if version of `comrak` was updated
- [ ] Ensure that `Cargo.lock` is updated by doing `rake clobber; rake compile`
- [ ] Ensure diff link above is up-to-date
- [ ] Check the release notes: https://gitlab.com/api/v4/projects/gitlab-org%2Fruby%2Fgems%2Fgitlab-glfm-markdown/repository/changelog?version=<NEW_VERSION>

Once merged, the fact that `lib/glfm_markdown/version.rb` was updated will cause CI to publish a new version
to RubyGems.

/label ~"type::maintenance" ~"static code analysis"
